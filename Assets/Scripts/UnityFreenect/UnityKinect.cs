﻿/* Enable Unsafe code on MONODEVELOP 5.9
 * update 01 giugno 2016 
 * https://groups.google.com/forum/#!topic/openkinect/Kfjx3MRshHA
 * LoadRawData : http://answers.unity3d.com/questions/555984/can-you-load-dds-textures-during-runtime.html
 * Doesn't work in Editor(crash), only work in Build x86_64 Linux 
 * -------------------- RENDER TEXTURE ---------------------------------------------------------------------------------------------------
 * Depth Textures Unity http://docs.unity3d.com/Manual/SL-DepthTextures.html
 * http://www.danielkiedrowski.com/2012/02/unity3d-how-to-render-depth-values-and-normals-to-a-rendertexture/
 * -------------------- CONVERT DEPTH FRAME FUNCTION ------------------------------------------------------------------------------------
 * https://digitalerr0r.wordpress.com/2011/06/21/kinect-fundamentals-3-getting-data-from-the-depth-sensor/
 * -------------------- KINECT ----------------------------------------------------------------------------------------------------------
 * http://www.i-programmer.info/ebooks/practical-windows-kinect-in-c/3802-using-the-kinect-depth-sensor.html
 * https://msdn.microsoft.com/en-us/library/jj131029.aspx
 * 
 * 
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using freenect;
using System.Threading;
using System;
using System.IO;
using System.Runtime.InteropServices;

public class UnityKinect : MonoBehaviour {

	public Text depthCamInfo;
	public Text videoCamInfo;
	public Text fps;
	public Text info;
	public Image RawKinectImage;
	public Image RawKinectDepthImage;
	public Kinect kinect;
	public int _w, width;
	public int _h, height;
	public byte[] pixels; 
	public float Fps = 60;

	private WebCamTexture PSCam = null;
	private Texture2D tex;
	private Texture2D texDepth;
	private Color32[] cols;

	public short[] raw; // array di short 16bit della Depth Kinect
	public ushort[] uRaw; // array di conversione


	void Start ()
	{
		InitKinect ();

		if (PSCam == null) 
		{
			WebCamDevice[] devices = WebCamTexture.devices;
			if (devices.Length > 0)
				PSCam = new WebCamTexture (devices [0].name);
		}

		//RawKinectDepthImage.material.mainTexture = PSCam;
		//PSCam.Play ();
		    
		tex = new Texture2D (_w, _h, TextureFormat.RGB24, false); // rgb24 w:1280 h:1024
		texDepth = new Texture2D (640, 480, TextureFormat.ARGB32, false); // Depth 11Bit 640x480

			_print ("VideoCamera  Mode: " + kinect.VideoCamera.Mode +
			"| Format : " + kinect.VideoCamera.Mode.Format +
			"| Res: " + kinect.VideoCamera.Mode.Resolution +
			"| Width: " + kinect.VideoCamera.Mode.Width + "| Height: " + kinect.VideoCamera.Mode.Height + "\n" +
			"| DepthCamera  Mode: " + kinect.DepthCamera.Mode +
			"| Format : " + kinect.DepthCamera.Mode.Format +
			"| Res: " + kinect.DepthCamera.Mode.Resolution +
			"| Width: " + kinect.DepthCamera.Mode.Width + "| Height: " + kinect.DepthCamera.Mode.Height + 
			"| Texture2D Format: " + texDepth.format);
	
	}
	private void HandleKinectDepthCameraDataReceived (object sender, DepthCamera.DataReceivedEventArgs e) 
	{
		depthCamInfo.text = "Depth ricevuta a "+ e.Timestamp.ToString();

		IntPtr handleDepth = e.Data.DataPointer;

		int w = e.Data.Width;  // 640
		int h = e.Data.Height; // 480
		width = w;
		height = h;
		// The DepthImageFrame data is presented as an array of short integers
		// i.e. each element is 16 bits, which is different from the byte array used in the VideoImageFrame.
		raw = new short[width * height]; 
		uRaw = new ushort[width * height];
		// Copia i dati da una matrice di short con segno a 16 bit gestita e unidimensionale a un puntatore di memoria non gestita https://msdn.microsoft.com/it-it/library/ms146628(v=vs.110).aspx
		Marshal.Copy (e.Data.DataPointer, raw, 0, raw.Length);

		// Preparo i colori 
		cols = new Color32[raw.Length];
		// ciclo nella depth grezza
		for(int i = 0; i < raw.Length; i++)
		{
			// assegno a ushort[] gli short 16bit provenienti dalla DepthCam
			// bit shift di 3 bit a destra perché dei 16bit provenienti dal kinect depth map i primi 3 sono per lo skeleton tracking
			uRaw[i] = (ushort)(raw[i] >> 3);

		    // test sui colori 
			if(( (byte)uRaw [i] > 0) && (byte)uRaw [i] < 100)
				cols [i].r = (byte)uRaw [i];
			if(( (byte)uRaw [i] > 100) && (byte)uRaw [i] < 120)
				cols [i].g = (byte)uRaw [i];
			if(( (byte)uRaw [i] > 120) && (byte)uRaw [i] < 255)
				cols [i].b = (byte)uRaw [i];
		}
	

	}
		

	private void HandleKinectVideoCameraDataReceived (object sender, BaseCamera.DataReceivedEventArgs e) 
	{
		//UnityEngine.Debug.LogError("Video ricevuto a "+e.Timestamp);
		videoCamInfo.text = "Video ricevuto a "+e.Timestamp.ToString();

		IntPtr handle = e.Data.DataPointer;

		int w = e.Data.Width; 
		int h = e.Data.Height; 
		_w = w;
		_h = h;
		// deve essere della stessa grandezza esatta del puntatore di freenect . la cam dovrebbe essere 640x640 x 3 RGB
		pixels = new byte[3 * w * h];
		Marshal.Copy (e.Data.DataPointer, pixels, 0, pixels.Length);

	
	}
		


	void InitKinect()
	{
		WriteLogFile(" - Device Count: " + Kinect.DeviceCount);
		if (Kinect.DeviceCount > 0) 
		{
			// Try to open a device
			kinect = new Kinect (0);
			kinect.Open ();
			WriteLogFile ("Kinect is Open: " + kinect.IsOpen);
			//preparo le funzioni che ricevono roba dalle camere
			kinect.VideoCamera.DataReceived += HandleKinectVideoCameraDataReceived;
			kinect.DepthCamera.DataReceived += HandleKinectDepthCameraDataReceived;

			//accendi le camere
			kinect.VideoCamera.Start();
			kinect.DepthCamera.Start();
			WriteLogFile ("VideoCamera is Running : " + kinect.VideoCamera.IsRunning);
			WriteLogFile ("DepthCamera is Running: " + kinect.DepthCamera.IsRunning);

			// Try to set LED colors
			kinect.LED.Color = LEDColor.Red;
			kinect.Motor.Tilt = 0;

			//mostra video modes per incodare disponibili 
			int i=0;
			foreach(var mode in kinect.VideoCamera.Modes)
			{
				switch (mode.Format) 
				{
					case VideoFormat.RGB:
						WriteLogFile("Video Mode "+i+": RGB");
						break;
					case VideoFormat.Infrared8Bit:
					    WriteLogFile("Video Mode "+i+": IR 8bit");
						break;
					case VideoFormat.Infrared10Bit:
					    WriteLogFile("Video Mode "+i+": IR 10bit");
						break;
					default:
						break;
				}
				i++;

			}
				


			//mostra depth modes
			i = 0;
			foreach(var mode in kinect.DepthCamera.Modes)
			{
				switch(mode.Format)
				{
					case DepthFormat.Depth10Bit:
						WriteLogFile("Depth Mode "+i+": 10 bit");
						break;
					case DepthFormat.Depth11Bit:
						WriteLogFile("Depth Mode "+i+": 11 bit");
						break;
					default:
						break;
				}
				i++;
			
			}

			// set the DepthCam and VideoCam Format
			//kinect.DepthCamera.Mode.Format = DepthFormat.Depth10Bit;

			Thread.Sleep (3000);
		}
	}
		
	void Update ()
	{
		//aggiorna sensori e motore
		kinect.UpdateStatus();
		//aggiorna gli eventi
		Kinect.ProcessEvents ();
		KeyFunction ();
		//Draw VideoCamera Kinect
		tex.LoadRawTextureData (pixels);
		tex.Apply ();
		RawKinectImage.material.mainTexture = tex;
	
		// Draw DepthCamera 
	    texDepth.SetPixels32 (cols); // SetPixels32 works only on ARGB32 texture formats. For other formats SetPixels32 is ignored
		texDepth.Apply ();
		RawKinectDepthImage.material.mainTexture = texDepth;

	}
		
	void LateUpdate() 
	{
		float interp = Time.deltaTime / (0.5f + Time.deltaTime);
		float currentFPS = 1.0f / Time.deltaTime;
		Fps = Mathf.Lerp(Fps, currentFPS, interp);
		fps.text = Mathf.RoundToInt(Fps).ToString ();

	}
	

	void KeyFunction()
	{

		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			kinect.Motor.Tilt = 1.0f;
			_print ("Kinect Motor Tilt: " + kinect.Motor.Tilt);
			Thread.Sleep(2000);

		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) 
		{
			kinect.Motor.Tilt = -1.0f;
			print ("Kinect Motor Tilt: " + kinect.Motor.Tilt);
			Thread.Sleep (2000);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			kinect.Motor.Tilt = 0.5f;
			print ("Kinect Motor Tilt: " + kinect.Motor.Tilt);
			Thread.Sleep (2000);
		}

		if (Input.GetKeyDown (KeyCode.L))
			kinect.LED.Color = LEDColor.BlinkRedYellow;

		if (Input.GetKeyDown (KeyCode.C)) 
			kinect.VideoCamera.DataReceived += HandleKinectVideoCameraDataReceived;
		
		if (Input.GetKey (KeyCode.Alpha0))
			kinect.VideoCamera.DataReceived -= HandleKinectVideoCameraDataReceived;

	}
		
	public static void WriteLogFile(string msg)
	{
		try
		{
			//Apre il log file visualizzando solo l'ultima riga valida del log.
			StreamWriter w = new StreamWriter("Testkinect.log", true);
			DateTime today = DateTime.Now;
			string line = string.Format("{0:00}-{1:00}-{2:0000}_{3:00}:{4:00}:{5:00} >{6}\n",
				today.Day, today.Month, today.Year, today.Hour, today.Minute, today.Second, msg);

			w.WriteLine(line);
			w.Close();
		}
		catch (Exception e)
		{
			UnityEngine.Debug.LogWarning(e.Message);
		}
	}

	public static void _print(string msg)
	{
		UnityEngine.Debug.LogError (msg);
		WriteLogFile (msg);
	}

	void OnApplicationQuit()
	{
		WriteLogFile("Shutdown the Kinect context " + Time.time + " seconds");	
		WriteLogFile("Application ending after " + Time.time + " seconds");

		kinect.LED.Color = LEDColor.BlinkGreen;
		kinect.Motor.Tilt = 0;
		// Shutdown the Kinect context
		kinect.Close ();
		Kinect.Shutdown();

	}

}
