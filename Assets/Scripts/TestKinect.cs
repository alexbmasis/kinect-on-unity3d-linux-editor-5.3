﻿using UnityEngine;
using System.Collections;
using System.Threading;
using freenect;
using System;


public class TestKinect : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.LogError("----------------------------------------");
		Debug.LogError("| Kinect.NET Wrapper Test              |");
		Debug.LogError("----------------------------------------\n");

		// Try to get number of devices connected
		Debug.LogError(" - Device Count: " + Kinect.DeviceCount);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.A)) 
		{
			Debug.LogError (" - Device Count: " + Kinect.DeviceCount);
		}
	
	}
}
