﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using freenect;
using System.Threading;
using System;

public class Prova2 : MonoBehaviour {


	public SwapBufferCollection<Byte> videoDataBuffers;
	public int frameDim;
	public Int16 multIR;

	public Kinect k;
	bool bNewVideo;
	bool bNewDepth;

	public IntPtr VideoBackBuffer
	{
		get{
			return this.videoDataBuffers.GetHandle(2);
		}
	}

	public VideoFrameMode videoMode = null;

	public VideoFrameMode VideoMode
	{
		get
		{
			return this.videoMode;
		}
		set
		{
			// Make sure we are actually changing something
			if(this.videoMode == value)
			{
				return;
			}

			// Resize buffer
			if(this.videoDataBuffers != null)
			{
				this.videoDataBuffers.Dispose(); 
			}
			this.videoDataBuffers = new SwapBufferCollection<byte>(3, 3 * value.Width * value.Height);

			// Save mode
			this.videoMode = value;
		}
	}

	void Start ()
	{
		Debug.LogError(" - Device Count: " + Kinect.DeviceCount);

		frameDim = 640 * 480;
		multIR = 50;
		bNewVideo = false;
		bNewDepth = false;

		Init ();


	}

	void Update ()
	{
		//aggiorna sensori e motore
		k.UpdateStatus();
		//aggiorna gli eventi
		Kinect.ProcessEvents ();

		renderVideo ();

		if (Input.GetKeyDown (KeyCode.Q)) 
			k.Close ();

		if (Input.GetKeyDown (KeyCode.W)) 
			Kinect.Shutdown();

		if (Input.GetKeyDown (KeyCode.A)) 
		{
			k.Motor.Tilt = 1;
			Thread.Sleep(3000);
		}

		if (Input.GetKeyDown (KeyCode.B)) 
		{
			k.Motor.Tilt = -1;
			Thread.Sleep(3000);
		}

		if (Input.GetKeyDown (KeyCode.C)) 
		{
			k.Motor.Tilt = 0;
			Thread.Sleep(3000);
		}



	}


	void Init()
	{
		if (Kinect.DeviceCount > 0) {

			// Try to open a device
			k = new Kinect (0);
			k.Open ();

			//preparo le funzioni che ricevono roba dalle camere
			k.VideoCamera.DataReceived += HandleKinectVideoCameraDataReceived;
			k.DepthCamera.DataReceived += HandleKinectDepthCameraDataReceived;

			//accendi le camere
			k.VideoCamera.Start();
			k.DepthCamera.Start();

			// Try to set LED colors

			k.LED.Color = LEDColor.Red;


			k.Motor.Tilt = 0;

			//mostra video modes disponibili
//			int i=0;
//   foreach(var mode in k.VideoCamera.Modes){
//    switch (mode.Format){
//    case VideoFormat.RGB:
//					UnityEngine.Debug.LogError("Video Mode "+i+": RGB");//0 1
//     break;
//    case VideoFormat.Infrared8Bit:
//					UnityEngine.Debug.LogError("Video Mode "+i+": IR 8bit");// 4 5
//     break;
//    case VideoFormat.Infrared10Bit:
//					UnityEngine.Debug.LogError("Video Mode "+i+": IR 10bit");//6 7
//     break;
//    default:
//      break;
//    }
//    i++;
//   }
//
//   //mostra depth modes
//   i=0;
//   foreach(var mode in k.DepthCamera.Modes){
//    switch(mode.Format){
//    case DepthFormat.Depth10Bit:
//					UnityEngine.Debug.LogError("Depth Mode "+i+": 10 bit");//1
//     break;
//    case DepthFormat.Depth11Bit:
//					UnityEngine.Debug.LogError("Depth Mode "+i+": 11 bit");//0
//     break;
//    default:
//     break;
//    }
//    i++;
//   }

//			this.k.VideoCamera.Stop();
//			var mode = (VideoFrameMode)VideoFormat.RGB;
//
//			this.k.VideoCamera.Mode = mode;
//			VideoMode = mode;
			//this.k.VideoCamera.Start();


			Thread.Sleep (3000);


		}


		// Shutdown the Kinect context
		//Kinect.Shutdown();

	}

	public void renderVideo(){
		if (bNewVideo) {
			videoDataBuffers.Swap(0,1);
		}

		bNewVideo = false;
	}

	//per ora non fanno nulla
	private void HandleKinectDepthCameraDataReceived (object sender, BaseCamera.DataReceivedEventArgs e){
		UnityEngine.Debug.LogError ("Depth ricevuta a "+e.Timestamp);

	}
	private void HandleKinectVideoCameraDataReceived (object sender, BaseCamera.DataReceivedEventArgs e){
		UnityEngine.Debug.LogError ("Video ricevuto a "+e.Timestamp);

		if (videoMode.Format == VideoFormat.Infrared10Bit) 
		{
			print (videoMode.Format.ToString ());
//			unsafe
//			{
//				Int16 * ptrMid = (Int16*)videoDataBuffers.GetHandle(1);
//				Int16 * ptrBack= (Int16*)videoDataBuffers.GetHandle(2);
//				for(int i=0;i<frameDim;i++)
//				{
//					*ptrMid++ = (Int16)(ptrBack[i]*multIR);
//				}
//			}
		}else{
			videoDataBuffers.Swap(1,2);
		}

		bNewVideo = true;

		k.VideoCamera.DataBuffer = VideoBackBuffer;
	}




}