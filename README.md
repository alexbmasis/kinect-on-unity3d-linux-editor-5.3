## Kinect LibFreenect for Unity Editor work on Linux ##
 
**Microsoft Kinect Freenect** libs wrapped into Unity3D 5.3.4 Linux version.

lib freenect compiled as .so and imported into Unity3D. Follow this in order to build libfreenect : (https://github.com/OpenKinect/libfreenect)


![kinectUnityLinux.png](https://bitbucket.org/repo/7boqkM/images/202781343-kinectUnityLinux.png)

### Features ###

* Motors
* LED
* Kinect RGB VideoCamera 
* Kinect DepthMap 
* [Learn More](https://robertofazio.com/)

### ToDO ###

* Accelerometer
* Audio